
import argparse
import os
import queue
import sounddevice as sd
import vosk
import sys
import random
import json
import time

from novelai_api import NovelAI_API
from novelai_api.Preset import Preset, Model

import generator as gen
import utilities as util

connections = {}
chatHistory = ["***"]

q = queue.Queue()

class Bot:
	def __init__(self, bot, mood, message_range):
		self.bot			= bot
		self.mood			= mood
		self.message_range	= message_range
		self.cur_messages	= 0
		self._chat_msg		= 0
		self.name			= None
		self.initial		= None
		self.voice			= None
		self.voice_seed		= None
		self.model			= None
		self.preset			= None
		self.prefix			= None
		self.context_window	= None
		self.context_mode	= None
		self.context_mode	= None
		self.gen_until_sen	= None
		self.name_bias		= None
		self.emoji_bias		= None
		self.use_string		= None
		self.num_logprobs	= None
		self.bias_dinkus	= None
		self.ban_brackets	= None
		self.ban_genji		= None
		self.wait_time		= None

		self.wants_to_chat		= False
		self.wants_to_chat_in	= None
		self.chat_range			= None

		self.config_load()
		print(f"{self.name} is connected to the server.")

	async def send_message(self, chat_history):
		time.sleep(self.wait_time)
		
		if self.model == "Euterpe":
			model_to_use = Model.Euterpe
		elif self.model == "Krake":
			model_to_use = Model.Krake
		else:
			model_to_use = Model.Sigurd

		channel_messages 	= chat_history
		message_result		= util.build_voice_context(channel_messages, self.bot, self.mood, model_to_use, self.context_mode, self.context_pos)
		
		api					= NovelAI_API()
		response			= await gen.generate(api, model_to_use, self.preset, self.prefix, message_result, 
												self.gen_until_sen, self.use_string, self.num_logprobs, self.ban_brackets, 
												self.bias_dinkus, self.ban_genji, self.bot, self.name_bias, self.emoji_bias)		

		return response

	def config_load(self):
		with open(f"./AI/{self.bot}/config.json", "r", encoding="utf-8") as config_file:
			data = json.load(config_file)
		self.name			= data["name"]
		self.initial		= data["initial"]
		self.voice			= data["voice"]
		self.voice_seed		= data["voice_seed"]
		self.model			= data["model"]
		self.preset			= Preset.from_file(data["preset"])
		self.prefix			= data["prefix"]
		self.context_window	= data["context_window"]
		self.context_mode	= data["context_mode"]
		self.context_pos	= data["context_pos"]
		self.gen_until_sen	= data["generate_until_sentence"]
		self.name_bias		= data["name_bias"]
		self.emoji_bias		= data["emoji_bias"]
		self.use_string		= data["use_string"]
		self.num_logprobs	= data["num_logprobs"]
		self.bias_dinkus	= data["bias_dinkus_asterism"]
		self.ban_brackets	= data["ban_brackets"]
		self.ban_genji		= data["ban_ambiguous_genji_tokens"]
		self.wait_time		= data["wait_time"]

	def int_or_str(self, text):
		"""Helper function for argument parsing."""
		try:
			return int(text)
		except ValueError:
			return text

	def callback(self, indata, frames, time, status):
		"""This is called (from a separate thread) for each audio block."""
		if status:
			print(status, file=sys.stderr)
		q.put(bytes(indata))

	async def main (self):
		global chatHistory
		parser = argparse.ArgumentParser(add_help=False)

		try:
			model = vosk.Model(lang="en-us")

			with sd.RawInputStream(samplerate=48000, blocksize = 8000, device=0, dtype='int16',
									channels=1, callback=self.callback):
					print('#' * 80)
					print('Press Ctrl+C to stop the recording')
					print('#' * 80)

					rec = vosk.KaldiRecognizer(model, 48000)
					while True:
						data = q.get()
						if rec.AcceptWaveform(data):
							test = json.loads(rec.Result())
							lastResult = f'''Gio:{test['text']}'''
							chatHistory.append(lastResult)
							if str(self.bot).lower() in lastResult:
								self.config_load()
								reply = await self.send_message(chatHistory)
								chatHistory.append(f"{self.bot}:{reply}")
							print(chatHistory)

		except KeyboardInterrupt:
			print('\nDone')
			parser.exit(0)
		except Exception as e:
			parser.exit(type(e).__name__ + ': ' + str(e))