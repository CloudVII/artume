from os import environ as env
from dotenv import load_dotenv
from threading import Timer
import time
import json
import argparse
import random
import inspect
import asyncio

import novelai_disc_instance
import koboldai_disc_instance

import generator as gen
import utilities as util

# Quick and dirty command parser check for AI user
parser = argparse.ArgumentParser(description="AI Chatbots, using NovelAI's API.")
parser.add_argument("-u", "--user",		dest="user",	type=str, 	help="user to login as; Bastet, Sekhmet, Nyx, Inari, craig",	 required=True)
parser.add_argument("-d", "--discord",	dest="discord",	type=bool,	help="login to discord")
parser.add_argument("-n", "--novelai", 	dest="novelai", type=bool,	help="use novelai API")
parser.add_argument("-k", "--koboldai", dest="koboldai",type=bool,	help="use koboldai API")
parser.add_argument("-v", "--voice",	dest="voice",	type=bool,	help="voice chat")
argv = parser.parse_args()

user 			= argv.user
boot_discord 	= argv.discord
boot_voice		= argv.voice
use_novelai		= argv.novelai
use_koboldai	= argv.koboldai

load_dotenv()

def main():
	if boot_discord:
		mood			= util.set_mood()
		message_range	= random.randrange(25,200)
		if use_novelai:
			bot = novelai_disc_instance.DiscordBot(user, mood, message_range)
		elif use_koboldai:

			with open("./AI/endpoint.json", "r", encoding="utf-8") as endpoint_file:
				endpoint_f = json.load(endpoint_file)

			endpoint = endpoint_f["endpoint"]

			bot = koboldai_disc_instance.DiscordBot(user, mood, message_range, endpoint)
		else:
			raise RuntimeError("An API is nessecary for the bot to run. duh.")

		bot.run(env[f'{user}_DISCORD_TOKEN'])

if __name__ == '__main__':
	main()
