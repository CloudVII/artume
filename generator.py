from os import environ as env
from enum import Enum, IntEnum, EnumMeta
from dotenv import load_dotenv

from novelai_api import NovelAI_API
from novelai_api.GlobalSettings import GlobalSettings
from novelai_api.Preset import Preset, Model
from novelai_api.BanList import BanList
from novelai_api.BiasGroup import BiasGroup
from novelai_api.Tokenizer import Tokenizer
from novelai_api.utils import b64_to_tokens

from logging import Logger, StreamHandler

import asyncio
import re
import json
import requests
import time

load_dotenv()

if "NAI_USERNAME" not in env or "NAI_PASSWORD" not in env:
	raise RuntimeError("Please ensure that NAI_USERNAME and NAI_PASSWORD are set in your environment")

username = env["NAI_USERNAME"]
password = env["NAI_PASSWORD"]

logger = Logger("NovelAI")
logger.addHandler(StreamHandler())

async def generate(api: NovelAI_API, model: Model, preset: str, mprefix: str, input: str, gen_until_sen: bool, use_string: bool, 
						num_logprobs: int, ban_brackets: bool, bias_dinkus: bool, ban_genji: bool, user: str, name_bias: float, emoji_bias: float):
	"""I want to rewrite this, not sure how yet.
	Args:
		api (NovelAI_API): _description_
		model (Model): Model to use.
		preset (Preset): Preset to use.
		mprefix: prefix to use. 
		input (str): input to use.
		gen_until_sen: whether to generate until the end of a sentence is found or not.
		use_string (bool): use string for input or tokenize ahead of time.
		num_logprobs: generates number of token probabilities
		ban_brackets: ban whether the ai can generate brackets in text
		bias_dinkus: bias against them being able to text with ***
		ban_genji: only really pertinent to genji model; garbage tokens
		user: the AI generating the message
		name_bias: the bias of the sibling name
	"""
	await api.high_level.login(username, password)

	ban_list = None
	ban_list1 = None
	model_context_limit = 2048
	
	global_settings		= GlobalSettings(generate_until_sentence = gen_until_sen, num_logprobs = num_logprobs,
										ban_brackets = ban_brackets, bias_dinkus_asterism = bias_dinkus, 
										ban_ambiguous_genji_tokens = ban_genji)

	if model == Model.Krake:
		ban_list1 = BanList().add([0],[2],[6],[7],[11],[12],[16],[30],[32],[60],[62],[64],[92],[93],[94], 
								[187],[209],[464],[544],[551],[578],[599],[654],[748],[768],[865],[876], 
								[880],[889],[910],[937],[986],[1092],[1217],[1380],[1570],[1713],[1926], 
								[2023],[2032],[2194],[2311],[2387],[2470],[2498],[3117],[3234],[3291],
								[3446],[3572],[3921],[4681],[4718],[4982],[5013],[5032],[5218],[5456],
								[5638,537],[5638,537,209],[5749],[6038],[6092],[6302],[6394],[6659],
								[6660],[6824],[6904],[7082],[7165],[7224],[7506],[7734],[8168],[8605],
								[8626],[8850],[9102],[9259],[9336],[9502],[9686],[9819],[9855],[9897],
								[10943],[11337],[11787],[12033],[12166],[12377],[12977],[13272],[13985],
								[14412],[14490],[14574],[14598],[15362],[15640],[16881],[17168],[17555],
								[17579],[18095],[18772],[20095],[20871],[20879],[21375],[21382],[21391],
								[21810],[21938],[22866],[23781],[24285],[25896],[26310],[26991],[28591],
								[29925],[31258],[31789],[31830],[32871],[34417],[34418],[35236],[35918],
								[36028],[36033],[36786],[39258],[39822],[41000],[41924],[45250],[46267],
								[46576],[47744],[48701],[31,28391],[28391]) 
	elif model == Model.Euterpe:
		ban_list1 = BanList().add([58],[60],[90],[92],[198],[685],[1391],[1782],[2361],[3693],[4083],[4357],
								[4895],[5512],[5974],[7131],[8183],[8351],[8762],[8964],[8973],[9063],
								[11208],[11709],[11907],[11919],[12878],[12962],[13018],[13412],[14631],
								[14692],[14980],[15090],[15437],[16151],[16410],[16589],[17241],[17414],
								[17635],[17816],[17912],[18083],[18161],[18477],[19629],[19779],[19953],
								[20520],[20598],[20662],[20740],[21476],[21737],[22133],[22241],[22345],
								[22935],[23330],[23785],[23834],[23884],[25295],[25597],[25719],[25787],
								[25915],[26076],[26358],[26398],[26894],[26933],[27007],[27422],[28013],
								[29164],[29225],[29342],[29565],[29795],[30072],[30109],[30138],[30866],
								[31161],[31478],[32092],[32239],[32509],[33116],[33250],[33761],[34171],
								[34758],[34949],[35944],[36338],[36463],[36563],[36786],[36796],[36937],
								[37250],[37913],[37981],[38165],[38362],[38381],[38430],[38892],[39850],
								[39893],[41832],[41888],[42535],[42669],[42785],[42924],[43839],[44438],
								[44587],[44926],[45144],[45297],[46110],[46570],[46581],[46956],[47175],
								[47182],[47527],[47715],[48600],[48683],[48688],[48874],[48999],[49074],
								[49082],[49146],[49946],[10221],[4841],[1427],[2602,834],[29343],[37405],
								[35780],[2602],[50256])

	if user == "Sekhmet":
		bias_list = BiasGroup(name_bias).add("Bastet",	" Bastet",	"Bastet ", 	" Bastet ", 	"bastet", 	" bastet", 	"bastet ", 	" bastet ",
											"Nyx",		" Nyx",		"Nyx ", 	" Nyx ", 		"nyx", 		" nyx", 	"nyx ", 	" nyx ",
											"Miko",		" Miko",	"Miko ",	" Miko ", 		"miko", 	" miko", 	"miko ", 	" miko ",
											"Craig",	" Craig",	"Craig ",	" Craig ",		"craig",	" craig",	"craig ",	" craig ")
	elif user == "Bastet":
		bias_list = BiasGroup(name_bias).add("Nyx",		" Nyx",		"Nyx ", 	" Nyx ", 		"nyx", 		" nyx", 	"nyx ", 	" nyx ",
											"Sekhmet", 	" Sekhmet",	"Sekhmet ", " Sekhmet ", 	"sekhmet", 	" sekhmet", "sekhmet ", " sekhmet ",
											"Miko",		" Miko",	"Miko ",	" Miko ", 		"miko", 	" miko", 	"miko ", 	" miko ",
											"Craig",	" Craig",	"Craig ",	" Craig ",		"craig",	" craig",	"craig ",	" craig ")
	elif user == "Nyx":
		bias_list = BiasGroup(name_bias).add("Bastet",	" Bastet",	"Bastet ", 	" Bastet ", 	"bastet", 	" bastet", 	"bastet ", 	" bastet ",
											"Sekhmet", 	" Sekhmet",	"Sekhmet ", " Sekhmet ", 	"sekhmet", 	" sekhmet", "sekhmet ", " sekhmet ",
											"Miko",		" Miko",	"Miko ",	" Miko ", 		"miko", 	" miko", 	"miko ", 	" miko ",
											"Craig",	" Craig",	"Craig ",	" Craig ",		"craig",	" craig",	"craig ",	" craig ")
	elif user == "Inari" or user == "Inari2":
		bias_list = BiasGroup(name_bias).add("Bastet",	" Bastet",	"Bastet ", 	" Bastet ", 	"bastet", 	" bastet", 	"bastet ", 	" bastet ",
											"Sekhmet", 	" Sekhmet",	"Sekhmet ", " Sekhmet ", 	"sekhmet", 	" sekhmet", "sekhmet ", " sekhmet ",
											"Nyx",		" Nyx",		"Nyx ", 	" Nyx ", 		"nyx", 		" nyx", 	"nyx ", 	" nyx ",
											"Craig",	" Craig",	"Craig ",	" Craig ",		"craig",	" craig",	"craig ",	" craig ")
	elif user == "craig":
		bias_list = BiasGroup(name_bias).add("Bastet",	" Bastet",	"Bastet ", 	" Bastet ", 	"bastet", 	" bastet", 	"bastet ", 	" bastet ",
											"Sekhmet", 	" Sekhmet",	"Sekhmet ", " Sekhmet ", 	"sekhmet", 	" sekhmet", "sekhmet ", " sekhmet ",
											"Nyx",		" Nyx",		"Nyx ", 	" Nyx ", 		"nyx", 		" nyx", 	"nyx ", 	" nyx ",
											"Miko",		" Miko",	"Miko ",	" Miko ", 		"miko", 	" miko", 	"miko ", 	" miko ")
	
	final_bias = [bias_list]

	if use_string == False:
		input = Tokenizer.encode(model, input)

	gen		= await api.high_level.generate(input, model, preset, global_settings, prefix = mprefix, biases = final_bias, bad_words = ban_list)
	result	= Tokenizer.decode(model, b64_to_tokens(gen["output"]))

	return result

def kb_generate(endpoint: str, input:str, max_context_length, max_length, n, rep_pen, rep_pen_range, rep_pen_slope, singleline,
				temperature, tfs, top_a, top_k, top_p, typical):
	r = requests.get(endpoint)
	print(r)
	headers = {"Content-Type": "application/json"}
	payload = json.dumps({
			"prompt": input,
			"max_context_length": 2048,
			"max_length": max_length,
			"n": n,
			"rep_pen": rep_pen,
			"rep_pen_range": rep_pen_range,
			"rep_pen_slope": rep_pen_slope,
			"singleline": singleline,
			"temperature": temperature,
			"tfs": tfs,
			"top_a": top_a,
			"top_k": top_k,
			"top_p": top_p,
			"typical": typical
			})

	while True:
		r = requests.post(endpoint + '/generate', data=payload.encode('utf-8'), headers=headers)

		if r.status_code == 200:
			break
		elif r.status_code == 503:
			print("Server is busy... waiting 1 second")
			time.sleep(3)
			print("Trying again.")
			continue
		else:
			print("???")

	result = r.json()
	final_result = result["results"][0]["text"]
	return final_result

async def generate_voice(api: NovelAI_API, input: str, voice: str, seed: int, message_id: int, user: str):
	await api.high_level.login(username, password)

	logger.info(f"Generating a tts voice for {len(input)} characters of text")

	tts = await api.low_level.generate_voice(input, voice, seed, True, "v2")
	with open(f"./AI/{user}/.result_voice/{message_id}_result.mp3", "wb") as f:
		f.write(tts)
