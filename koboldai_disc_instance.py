import random
import json
import time

from novelai_api.Preset import Preset, Model

from	discord.ext			import commands
from	discord.commands	import ApplicationContext, option
import	discord

import generator as gen
import utilities as util

connections = {}

class DiscordBot(commands.Bot):

	def __init__(self, bot, mood, message_range, endpoint):
		super().__init__(command_prefix="!", intents=discord.Intents.all())
		self.bot			= bot
		self.mood			= mood
		self.message_range	= message_range
		self.endpoint		= endpoint
		self.cur_messages	= 0
		self._chat_msg		= 0
		self.name			= None
		self.initial		= None
		self.context_window	= None
		self.context_mode	= None
		self.context_pos	= None
		self.can_chat		= None
		self.general_chat	= None
		self.twitter		= None
		self.max_context	= None
		self.max_length		= None
		self.msgs_to_gen	= None
		self.rep_pen		= None
		self.rep_range		= None
		self.rep_slope		= None
		self.singleline		= None
		self.temp			= None
		self.tfs			= None
		self.top_a			= None
		self.top_k			= None
		self.top_p			= None
		self.typical		= None

		self.wants_to_chat		= False
		self.wants_to_chat_in	= None
		self.chat_range			= None

		self.config_load()

	
	async def on_ready(self):
		print(f"Bot {self.user.display_name} is connected to server.")


	async def send_message(self, server, channel, message, direct_message, twotter):
		self.config_load()
		model_to_use = Model.Krake
		
		if direct_message: 
			channel_messages 	= await channel.DMChannel.history(limit=self.context_window).flatten()
		else:
			channel_messages 	= await channel.history(limit=self.context_window).flatten()
		message_result		= util.build_context(channel_messages, self.bot, self.mood, server, channel, model_to_use, self.context_mode, self.context_pos, twotter)
		
		response			= gen.kb_generate(self.endpoint, message_result, self.max_context, self.max_length, self.msgs_to_gen, self.rep_pen, self.rep_range, self.rep_slope,
												self.singleline, self.temp, self.tfs, self.top_a, self.top_k, self.top_p, self.typical)		
		
		await channel.send(response)

		with open(f"./AI/{self.bot}/.result/{message.id}_result.txt", "w", encoding="utf-8") as f:
			f.write(f"Settings used:\n"
					f"context window: {self.context_window}\n"
					"***\n"
					"Output:\n"
					f"{message_result}{response}")

	async def on_message(self, message):
		mentioned = False
		direct_message = False

		direct_message = False
		guild 		= message.guild
		channel_ID 	= message.channel.id
		channel 	= self.get_channel(channel_ID)
		gen_channel = self.get_channel(1002340828599038042)

		min_roll = 1
		max_roll = 1000
		desired_roll = 995

		skill_check = util.rand_check(min_roll, max_roll, desired_roll)

		if self.bot == "Sekhmet":
			send_another_message = util.rand_check(min_roll, max_roll, 900)
		elif self.bot == "Bastet":
			send_another_message = util.rand_check(min_roll, max_roll, 820)
		elif self.bot ==  "Miko":
			send_another_message = util.rand_check(min_roll, max_roll, 800)
		elif self.bot == "craig":
			send_another_message = util.rand_check(min_roll, max_roll, 900)
		elif self.bot == "Nyx":
			send_another_message = util.rand_check(min_roll, max_roll, 850)
		elif self.bot == "Izuna":
			send_another_message = util.rand_check(min_roll, max_roll, 800)

		twotter_check = util.rand_check(min_roll, 100, 95)

		for i in self.name:
			if i in message.content:
				mentioned = True

		if message.author == self.user and not send_another_message:
			return
		else:
			
			if self.user.mentioned_in(message) or mentioned:
				self.cur_messages = self.cur_messages + 1

				if self.cur_messages >= self.message_range:
					self.message_range	= random.randrange(25, 200)
					self.mood			= util.set_mood()
					print(f"New mood; {self.mood}")
					self.cur_messages	= 0

				await self.send_message(guild, channel, message, direct_message, False)

			if send_another_message:
				guild 		= message.guild
				channel_ID 	= message.channel.id
				channel 	= self.get_channel(channel_ID)
			
				if self.cur_messages >= self.message_range:
						self.message_range	= random.randrange(25, 200)
						self.mood			= util.set_mood()
						print(f"New mood; {self.mood}")
						self.cur_messages	= 0

				await self.send_message(guild, channel, message, direct_message, False)

	def config_load(self):
		with open(f"./AI/{self.bot}/config.json", "r", encoding="utf-8") as config_file:
			data = json.load(config_file)
		self.name			= data["name"]
		self.initial		= data["initial"]
		self.context_window	= data["context_window"]
		self.context_mode	= data["context_mode"]
		self.context_pos	= data["context_pos"]
		self.can_chat		= data["can_chat"]
		self.general_chat	= data["general_chat"]
		self.twitter		= data["twitter"]
		self.max_context	= data["koboldai"]["max_context_length"]
		self.max_length		= data["koboldai"]["max_length"]
		self.msgs_to_gen	= data["koboldai"]["msgs_to_gen"]
		self.rep_pen		= data["koboldai"]["rep_pen"]
		self.rep_range		= data["koboldai"]["rep_pen_range"]
		self.rep_slope		= data["koboldai"]["rep_pen_slope"]
		self.singleline		= data["koboldai"]["singleline"]
		self.temp			= data["koboldai"]["temperature"]
		self.tfs			= data["koboldai"]["tfs"]
		self.top_a			= data["koboldai"]["top_a"]
		self.top_k			= data["koboldai"]["top_k"]
		self.top_p			= data["koboldai"]["top_p"]
		self.typical		= data["koboldai"]["typical"]
