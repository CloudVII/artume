import random
import json
import time

from novelai_api import NovelAI_API
from novelai_api.Preset import Preset, Model
import discord
from discord.ext import commands
from discord.commands import ApplicationContext, option

import generator as gen
import utilities as util

connections = {}


class DiscordBot(commands.Bot):

	def __init__(self, bot, mood, message_range):
		super().__init__(command_prefix="!", intents=discord.Intents.all())
		self.bot			= bot
		self.mood			= mood
		self.message_range	= message_range
		self.cur_messages	= 0
		self._chat_msg		= 0
		self.name			= None
		self.initial		= None
		self.voice			= None
		self.voice_seed		= None
		self.model			= None
		self.preset			= None
		self.prefix			= None
		self.context_window	= None
		self.context_mode	= None
		self.gen_until_sen	= None
		self.name_bias		= None
		self.emoji_bias		= None
		self.use_string		= None
		self.num_logprobs	= None
		self.bias_dinkus	= None
		self.ban_brackets	= None
		self.ban_genji		= None
		self.wait_time		= None
		self.can_chat		= None
		self.general_chat	= None
		self.twitter		= None

		self.wants_to_chat		= False
		self.wants_to_chat_in	= None
		self.chat_range			= None

		self.config_load()

	async def on_ready(self):
		print(f"Bot {self.user.display_name} is connected to server.")

	
	async def send_message(self, server, channel, message, twotter):
		time.sleep(self.wait_time)
		self.config_load()
		
		if self.model == "Euterpe":
			model_to_use = Model.Euterpe
		elif self.model == "Krake":
			model_to_use = Model.Krake
		else:
			model_to_use = Model.Sigurd

		channel_messages 	= await channel.history(limit=self.context_window).flatten()
		message_result		= util.build_context(channel_messages, self.bot, self.mood, server, channel, model_to_use, self.context_mode, self.context_pos, twotter)
		
		api					= NovelAI_API()
		response			= await gen.generate(api, model_to_use, self.preset, self.prefix, message_result, 
												self.gen_until_sen, self.use_string, self.num_logprobs, self.ban_brackets, 
												self.bias_dinkus, self.ban_genji, self.bot, self.name_bias, self.emoji_bias)		
		
		await channel.send(response)

		with open(f"./AI/{self.bot}/.result/{message.id}_result.txt", "w", encoding="utf-8") as f:
			f.write(f"Settings used:\n"
					f"Model: {self.model}, {self.preset}, Prefix: {self.prefix}\n"
					f"context window: {self.context_window}\n"
					f"generate until sentence: {self.gen_until_sen}\n"
					f"use string: {self.use_string}\n"
					f"num logprobs: {self.num_logprobs}\n"
					f"ban brackets: {self.ban_brackets}\n"
					f"bias dinkus asterism: {self.bias_dinkus}\n"
					f"ban ambiguous genji tokens: {self.ban_genji}\n"
					"***\n"
					"Output:\n"
					f"{message_result}{response}")

	async def on_message(self, message):
		mentioned = False

		min_roll = 1
		max_roll = 1000
		desired_roll = 995

		skill_check = util.rand_check(min_roll, max_roll, desired_roll)

		if self.bot == "Sekhmet":
			send_another_message = util.rand_check(min_roll, max_roll, 900)
		elif self.bot == "Bastet":
			send_another_message = util.rand_check(min_roll, max_roll, 820)
		elif self.bot ==  "Inari" or self.bot == "Inari2":
			send_another_message = util.rand_check(min_roll, max_roll, 870)
		elif self.bot == "craig":
			send_another_message = util.rand_check(min_roll, max_roll, 900)
		elif self.bot == "Nyx":
			send_another_message = util.rand_check(min_roll, max_roll, 850)

		twotter_check = util.rand_check(min_roll, 100, 95)

		for i in self.name:
			if i in message.content:
				mentioned = True
		
		if message.author == self.user and send_another_message:
			guild 		= message.guild
			channel_ID 	= message.channel.id
			channel 	= self.get_channel(channel_ID)

			if self.cur_messages >= self.message_range:
					self.message_range	= random.randrange(25, 200)
					self.mood			= util.set_mood()
					print(f"New mood; {self.mood}")
					self.cur_messages	= 0

			if self.wants_to_chat == False:
				self.wants_to_chat = util.rand_check(1, 200, 185)
				self.chat_range 		= random.randrange(5, 25)
				self.wants_to_chat_in = channel
			else:
				self._chat_msg	= self._chat_msg + 1
				self.wait_time	= self.wait_time + 0.45
				if self._chat_msg >= self.chat_range:
					print("I don't feel like chatting anymore.")
					self._chat_msg 		= 0
					self.wants_to_chat 	= False

			await self.send_message(guild, channel, message, False)
		elif message.author == self.user and not send_another_message:
			return
		else:
			if message.content.startswith(f"{self.initial}TTS"):
				ref_message		= await message.channel.fetch_message(message.reference.message_id)
				api				= NovelAI_API()
				gen_voice		= await gen.generate_voice(api, ref_message.content, self.voice, self.voice_seed, message.id, self.bot)
				await message.channel.send(file=discord.File(f"./AI/{self.bot}/.result_voice/{message.id}_result.mp3"))

			elif self.user.mentioned_in(message) or mentioned:
				self.cur_messages = self.cur_messages + 1

				guild 		= message.guild
				channel_ID 	= message.channel.id
				channel 	= self.get_channel(channel_ID)
				gen_channel = self.get_channel(1002340828599038042)

				if self.cur_messages >= self.message_range:
					self.message_range	= random.randrange(25, 200)
					self.mood			= util.set_mood()
					print(f"New mood; {self.mood}")
					self.cur_messages	= 0

				if self.wants_to_chat == False:
					self.wants_to_chat = util.rand_check(1, 200, 185)
					self.chat_range 		= random.randrange(5, 25)
					self.wants_to_chat_in = channel
				else:
					self._chat_msg	= self._chat_msg + 1
					self.wait_time	= self.wait_time + 0.45
					if self._chat_msg >= self.chat_range:
						print("I don't feel like chatting anymore.")
						self._chat_msg 		= 0
						self.wants_to_chat 	= False

				await self.send_message(guild, channel, message, False)

			elif skill_check and self.general_chat == True:
				self.cur_messages = self.cur_messages + 1
				guild 			= self.get_guild(72141602633547776)
				channel			= self.get_channel(1002340828599038042)

				if self.cur_messages >= self.message_range:
					self.message_range	= random.randrange(25, 200)
					self.mood			= util.set_mood()
					print(f"New mood; {self.mood}")
					self.cur_messages	= 0

				if self.wants_to_chat == False:
					self.wants_to_chat = util.rand_check(1, 200, 185)
					self.chat_range 		= random.randrange(5, 25)
					self.wants_to_chat_in = channel
				else:
					self._chat_msg	= self._chat_msg + 1
					self.wait_time	= self.wait_time + 0.45
					if self._chat_msg >= self.chat_range:
						print("I don't feel like chatting anymore.")
						self._chat_msg 		= 0
						self.wants_to_chat 	= False
				
				await self.send_message(72141602633547776, channel, message, False)

			elif self.can_chat and self.wants_to_chat:
				self.cur_messages = self.cur_messages + 1
				guild 			= self.get_guild(72141602633547776)
				channel			= self.get_channel(1002340828599038042)

				if self.cur_messages >= self.message_range:
					self.message_range	= random.randrange(25, 200)
					self.mood			= util.set_mood()
					print(f"New mood; {self.mood}")
					self.cur_messages	= 0

				self._chat_msg	= self._chat_msg + 1
				self.wait_time	= self.wait_time + 0.45
				if self._chat_msg >= self.chat_range:
					print("I don't feel like chatting anymore.")
					self._chat_msg 		= 0
					self.wants_to_chat 	= False
			
				await self.send_message(72141602633547776, channel, message, False)

		if twotter_check and self.twitter == True:
			guild 			= self.get_guild(72141602633547776)
			
			channel_name 	= None
			channel 		= None
        
			if self.bot == "Sekhmet":
				channel_name = "sekhmet"
				channel = discord.utils.get(guild.text_channels, name=channel_name)
			elif self.bot == "Bastet":
				channel_name = "bastet"
				channel = discord.utils.get(guild.text_channels, name=channel_name)
			elif self.bot ==  "Inari" or self.bot == "Inari2":
				channel_name = "miko"
				channel = discord.utils.get(guild.text_channels, name=channel_name)
			elif self.bot == "craig":
				channel_name = "craig"
				channel = discord.utils.get(guild.text_channels, name=channel_name)
			elif self.bot == "Nyx":
				channel_name = "nyx"
				channel = discord.utils.get(guild.text_channels, name=channel_name)
            
			#channel = discord.utils.get(guild.text_channels, name=channel_name)
        
			await self.send_message(72141602633547776, channel, message, True)


	def config_load(self):
		with open(f"./AI/{self.bot}/config.json", "r", encoding="utf-8") as config_file:
			data = json.load(config_file)
		self.name			= data["name"]
		self.initial		= data["initial"]
		self.voice			= data["novelai"]["voice"]
		self.voice_seed		= data["novelai"]["voice_seed"]
		self.model			= data["novelai"]["model"]
		self.preset			= Preset.from_file(data["novelai"]["preset"])
		self.prefix			= data["novelai"]["prefix"]
		self.context_window	= data["context_window"]
		self.context_mode	= data["context_mode"]
		self.context_pos	= data["context_pos"]
		self.gen_until_sen	= data["novelai"]["generate_until_sentence"]
		self.name_bias		= data["novelai"]["name_bias"]
		self.emoji_bias		= data["novelai"]["emoji_bias"]
		self.use_string		= data["novelai"]["use_string"]
		self.num_logprobs	= data["novelai"]["num_logprobs"]
		self.bias_dinkus	= data["novelai"]["bias_dinkus_asterism"]
		self.ban_brackets	= data["novelai"]["ban_brackets"]
		self.ban_genji		= data["novelai"]["ban_ambiguous_genji_tokens"]
		self.wait_time		= data["novelai"]["wait_time"]
		self.can_chat		= data["can_chat"]
		self.general_chat	= data["general_chat"]
		self.twitter		= data["twitter"]
