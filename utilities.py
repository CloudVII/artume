import random
import json
import re

from novelai_api.Tokenizer import Tokenizer
from novelai_api.Preset import Preset, Model

def build_context(context, user: str, mood: str, guild, channel, model, context_mode, context_pos, twotter):
	model_context_limit = 2048
	chat_msgs			= None
	user_id				= None

	if user == "Bastet":
		user_id = "<@966508749122580541>"
	elif user == "Sekhmet":
		user_id = "<@967277591906123836>"
	elif user == "Nyx":
		user_id = "<@970061055818629121>"
	elif user == "Miko":
		user_id = "<@971578240051347486>"
	elif user == "Izuna":
		user_id = "<@1017605022160261160>"
	elif user == "craig":
		user_id = "<@971997128073285652>"
	
	with open(f"./AI/{user}/config.json", "r", encoding="utf-8") as config_file:
		data = json.load(config_file)
	with open(f"./AI/{user}/descriptor_txt.txt", "r", encoding="utf-8") as desc_file:
		desc_txt = desc_file.read()
	with open("./AI/users.json", "r", encoding="utf-8") as user_file:
			users = json.load(user_file)
	with open(data["context_txt"], "r", encoding="utf-8") as memory_file:
		memory = json.load(memory_file)

	prime_memory 	= "\n".join(memory["Primary Memory"])
	
	context.reverse()
	l = []
	for i in context:
		dummy = f"{i.author}: {i.content}"

		l.append(dummy)
		chat_msgs = "\n".join(l)
	
	chat_msgs		= chat_msgs.replace(user_id, user)

	chat_msgs	= re.sub("#....: ", ':', chat_msgs)

	for i in users["user"]:
		chat_msgs	= chat_msgs.replace(i["id"], i["name"])

	chat_msgs		= chat_msgs.replace("Nyxx", 	"Nyx")
	chat_msgs		= chat_msgs.replace("Inari",	"Miko")
	chat_msgs		= chat_msgs.replace("Thwack",	"Gio")

	split_chat		= chat_msgs.rsplit("\n")
	split_chat.insert(len(split_chat)-3, f"{desc_txt}{guild}' in the channel: {channel}")
	split_chat.insert(len(split_chat)-3, f"{user} is feeling{mood}")

	if twotter == True:
		new_thought = set_thoughts()
		split_chat.insert(len(split_chat), f'''{user} is thinking about {new_thought}, and wants to talk about it.''')

	chat_msgs			= "\n".join(split_chat)

	if context_mode == 2:
		for key in memory:
			value = memory[key]
			string = None
			if key in chat_msgs:
				string = "\n".join(value)
				string = "\n" + string
				prime_memory = prime_memory + string
		prime_memory	= prime_memory + "\n" + "***"
		chat_msgs		= trim_to_context_limit(model, model_context_limit, chat_msgs, prime_memory)
		split_chat		= chat_msgs.rsplit("\n")
		split_chat.insert(len(split_chat)-context_pos, f"{prime_memory}")
	
	chat_msgs	= "\n".join(split_chat)
	chat_msgs	= chat_msgs + f"\n{user}:"

	if context_mode == 1:
		final_context = prime_memory + chat_msgs
	if context_mode == 2:
		final_context = chat_msgs

	return final_context

def build_kb_context(context, user: str, mood: str, guild, channel, model, context_mode, context_pos, twotter):
	model_context_limit = 2048
	chat_msgs			= None
	user_id				= None

	if user == "Bastet":
		user_id = "<@966508749122580541>"
	elif user == "Sekhmet":
		user_id = "<@967277591906123836>"
	elif user == "Nyx":
		user_id = "<@970061055818629121>"
	elif user == "Miko":
		user_id = "<@971578240051347486>"
	elif user == "Izuna":
		user_id = "<@1017605022160261160>"
	elif user == "craig":
		user_id = "<@971997128073285652>"
	
	with open(f"./AI/{user}/config.json", "r", encoding="utf-8") as config_file:
		data = json.load(config_file)
	with open(f"./AI/{user}/descriptor_txt.txt", "r", encoding="utf-8") as desc_file:
		desc_txt = desc_file.read()
	with open("./AI/users.json", "r", encoding="utf-8") as user_file:
		users = json.load(user_file)
	with open(data["context_txt"], "r", encoding="utf-8") as memory_file:
		memory = json.load(memory_file)
		
	prime_memory 	= "\n".join(memory["Primary Memory"])
	
	context.reverse()
	l = []
	for i in context:
		dummy = f"{i.author}: {i.content}"

		l.append(dummy)
		chat_msgs = "\n".join(l)
	
	chat_msgs		= chat_msgs.replace(user_id, user)

	chat_msgs	= re.sub("#....: ", ':', chat_msgs)

	for i in users["user"]:
		chat_msgs	= chat_msgs.replace(i["id"], i["name"])

	chat_msgs		= chat_msgs.replace("Nyxx", 	"Nyx")
	chat_msgs		= chat_msgs.replace("Inari",	"Miko")
	chat_msgs		= chat_msgs.replace("Thwack",	"Gio")

	split_chat		= chat_msgs.rsplit("\n")
	split_chat.insert(len(split_chat)-3, f"{desc_txt}{guild}' in the channel: {channel}")
	split_chat.insert(len(split_chat)-3, f"{user} is feeling{mood}")

	if twotter == True:
		new_thought = set_thoughts()
		split_chat.insert(len(split_chat), f'''{user} is thinking about {new_thought}, and wants to talk about it.''')

	chat_msgs			= "\n".join(split_chat)

	if context_mode == 2:
		for key in memory:
			value = memory[key]
			string = None
			if key in chat_msgs:
				string = "\n".join(value)
				string = "\n" + string
				prime_memory = prime_memory + string
		prime_memory	= prime_memory + "\n" + "***"
		split_chat		= chat_msgs.rsplit("\n")
		split_chat.insert(len(split_chat)-context_pos, f"{prime_memory}")
	
	chat_msgs	= "\n".join(split_chat)
	chat_msgs	= chat_msgs + f"\n{user}:"

	if context_mode == 1:
		final_context = prime_memory + chat_msgs
	if context_mode == 2:
		final_context = chat_msgs

	return final_context

def build_voice_context(context, user: str, mood: str, model, context_mode, context_pos):
	model_context_limit = 1840
	chat_msgs			= None
	user_id				= None

	if user == "Bastet":
		user_id = "<@966508749122580541>"
	elif user == "Sekhmet":
		user_id = "<@967277591906123836>"
	elif user == "Nyx":
		user_id = "<@970061055818629121>"
	elif user == "Miko":
		user_id = "<@971578240051347486>"
	elif user == "craig":
		user_id = "<@971997128073285652>"
	
	with open(f"./AI/{user}/config.json", "r", encoding="utf-8") as config_file:
		data = json.load(config_file)
	with open(f"./AI/{user}/descriptor_txt.txt", "r", encoding="utf-8") as desc_file:
		desc_txt = desc_file.read()
	with open("./AI/users.json", "r", encoding="utf-8") as user_file:
		users = json.load(user_file)
	with open(data["context_txt"], "r", encoding="utf-8") as memory_file:
		memory = json.load(memory_file)

	prime_memory 	= "\n".join(memory["Primary Memory"])

	chat_msgs = "\n".join(context)
	chat_msgs		= chat_msgs.replace(user_id, user)

	chat_msgs	= re.sub("#....: ", ':', chat_msgs)

	for i in users["user"]:
		chat_msgs	= chat_msgs.replace(i["id"], i["name"])

	chat_msgs		= chat_msgs.replace("Nyxx", 	"Nyx")
	chat_msgs		= chat_msgs.replace("Inari",	"Miko")
	chat_msgs		= chat_msgs.replace("Thwack",	"Gio")

	split_chat		= chat_msgs.rsplit("\n")
	split_chat.insert(len(split_chat)-3, f"{user} is feeling{mood}")

	chat_msgs			= "\n".join(split_chat)

	if context_mode == 2:
		for key in memory:
			value = memory[key]
			string = None
			if key in chat_msgs:
				string = "\n".join(value)
				string = "\n" + string
				prime_memory = prime_memory + string
		prime_memory	= prime_memory + "\n" + "***"
		chat_msgs		= trim_to_context_limit(model, model_context_limit, chat_msgs, prime_memory)
		split_chat		= chat_msgs.rsplit("\n")
		split_chat.insert(len(split_chat)-context_pos, f"{prime_memory}")
	
	chat_msgs	= "\n".join(split_chat)
	chat_msgs	= chat_msgs + f"\n{user}:"

	if context_mode == 1:
		final_context = prime_memory + chat_msgs
	if context_mode == 2:
		final_context = chat_msgs

	return final_context

def trim_to_context_limit(model, model_context_limit, messages, memory):
	chat_tokenized		= Tokenizer().encode(model, messages)
	memory_tokenized	= Tokenizer().encode(model, memory)
	overall_length		= len(memory_tokenized + chat_tokenized)
	padding				= 0

	print("memory, input: ", overall_length)

	if overall_length > 2048:
		length_to_cut = abs(model_context_limit - overall_length) + padding
		print("length to cut: ", length_to_cut)

		for i in range(0, length_to_cut):
			chat_tokenized.pop(0)

		print("final count:", len(memory_tokenized + chat_tokenized))
		result_of_cut	= Tokenizer.decode(model, chat_tokenized)
		messages		= result_of_cut
	
	return messages

def set_thoughts():
	with open(f"./AI/thoughts.json") as thoughts_file:
		thoughts_data = json.load(thoughts_file)
	
	cur_thought		= random.choices(thoughts_data["thought"])
	new_thought		= f"{cur_thought}"
	new_thought		= re.sub("[\[\]']+","", new_thought)

	return new_thought

def set_mood():
	with open(f"./AI/moods.json") as mood_file:
		mood_data = json.load(mood_file)

	cur_mood		= random.choices(mood_data["mood"])
	cur_modifier	= random.choices(mood_data["modifier"])

	new_mood		= f"{cur_modifier} {cur_mood}"
	new_mood		= re.sub("[\[\]']+","", new_mood)
	print(f"New mood:{new_mood}")
	return new_mood

def rand_check(min, max, desired):
	check = False

	roll = random.randint(min, max)
	if roll >= desired:
		check = True
	
	return check