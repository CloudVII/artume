# Artume

Very experimental chatbots using NovelAI's REST API; currently they exclusively run on discord, want to extend it and make them more agnostic.

## Setup
- install requirements via requirements.txt

The python wrapper for NovelAI's REST API hasn't been updated to support krake-v2, you'll need to modify it and add additional files:
- in novelai-api's Preset.py, change the model class's Krake entry from "krake-v1" to "krake-v2"
- novelai-api requires local preset fiels for each model
-- in novelai-api/presets, create a new folder named "presets_krake_v2"
-- download the krake_v2 presets from NovelAI and place them in presets_krake_v2

## Roadmap
- [ ] Major refactor and cleanup, code quality is disastrous
- [ ] Generate config if it doesn't exist
- [ ] Better audio generation implementation
- [ ] Voice chat
- [ ] Twotter channel(s)
- [ ] Flesh out everything
- [ ] ???

## License
[MIT](https://choosealicense.com/licenses/mit/)